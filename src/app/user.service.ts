import { Injectable } from '@angular/core';
import * as faker from 'faker';

export interface User {
  id: number;
  name: string;
  surname: string;
  age: number;
  date: Date;
}

@Injectable({
  providedIn: 'root',
})
export class UserService {
  users: User[] = [];

  constructor() {
    for (let index = 0; index < 10; index++) {
      const user: User = {
        id: index,
        name: faker.name.findName(),
        surname: faker.name.lastName(),
        age: faker.random.number(35),
        date: faker.date.past(),
      };

      this.users.push(user);
    }
  }
}
