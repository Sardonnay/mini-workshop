import { UserCardComponent } from './user-card/user-card.component';
import { UpdateComponent } from './update/update.component';
import { LoadingComponent } from './loading/loading.component';
import { AddingComponent } from './adding/adding.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'add',
    component: AddingComponent,
  },
  {
    path: 'user',
    component: LoadingComponent,
  },
  {
    path: 'user/{id}',
    component: LoadingComponent,
  },
  {
    path: 'update',
    component: UpdateComponent,
  },
  {
    path: '**',
    component: UserCardComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
