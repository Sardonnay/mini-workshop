import { ActionReducerMap } from '@ngrx/store';
import * as fromUser from '../user/user.reducer';
import * as fromApp from '../app/app.reducer';

export interface State {
  user: fromUser.State;
  appData: fromApp.State;
}

export const reducers: ActionReducerMap<Readonly<State>> = {
  user: fromUser.reducer,
  appData: fromApp.reducer,
};
