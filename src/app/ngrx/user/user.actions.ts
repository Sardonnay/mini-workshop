import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/user.service';

export const ReqAllUsers = createAction('[Loading Component] ReqAllUsers');
export const AllUsersFail = createAction('[ReqAllUsers] AllUsersFail');
export const AllUsersSuccess = createAction(
  '[ReqAllUsers] AllUsersSuccess',
  props<{ users: User[] }>()
);
