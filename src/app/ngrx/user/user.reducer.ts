import { User } from './../../user.service';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import * as UserActions from './user.actions';

export interface UserTime extends User {
  currentDate: Date;
}

export const userFeatureKey = 'user';
export interface State extends EntityState<User> {
  userWithDate: UserTime;
}

export const adapter: EntityAdapter<User> = createEntityAdapter({
  selectId: selectUserId,
});

export function selectUserId(user: User): number {
  return user.id;
}

const initialState: State = adapter.getInitialState<State>({
  ids: [],
  entities: {},
  userWithDate: null,
});

export const reducer = createReducer(
  initialState,
  on(UserActions.AllUsersSuccess, (state, payload) => {
    return adapter.upsertMany(payload.users, state);
  })
);
