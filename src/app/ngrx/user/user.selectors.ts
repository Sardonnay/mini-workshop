import { User } from './../../user.service';
import { Dictionary } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromUser from './user.reducer';

export const selectUserState = createFeatureSelector<fromUser.State>(
  fromUser.userFeatureKey
);
export const {
  selectEntities: geUserEntities,
  selectAll: getAllUsers,
} = fromUser.adapter.getSelectors(selectUserState);

export const getUserById = createSelector(
  geUserEntities,
  (dictUsers: Dictionary<User>, props: { id: number }) => {
    const user = dictUsers[props.id];

    return user || null;
  }
);
