import { UserEffects } from './ngrx/user/user.effects';
import { AppEffects } from './ngrx/app/app.effects';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { LoadingComponent } from './loading/loading.component';
import { AddingComponent } from './adding/adding.component';
import { UpdateComponent } from './update/update.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import * as fromRoot from 'src/app/ngrx/reducers/index';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { UserCardComponent } from './user-card/user-card.component';

@NgModule({
  declarations: [
    AppComponent,
    LoadingComponent,
    AddingComponent,
    UpdateComponent,
    UserCardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    EffectsModule.forRoot([AppEffects, UserEffects]),
    StoreModule.forRoot(fromRoot.reducers, {}),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
    NgbModule,
    !environment.production
      ? StoreDevtoolsModule.instrument({
          maxAge: 225, // Retains last 25 states
          logOnly: environment.production, // Restrict extension to log-only mode
        })
      : [],
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
